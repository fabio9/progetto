#!/usr/bin/env python

from __future__ import print_function

#list of substacks
substacks = dict()

from bcfind import sub
import argparse
import time
import tables
import cPickle as pickle
import numpy as np
import timeit
import datetime
import platform
import json
import os, fcntl
import errno
import sys
import math
from progressbar import *
from bcfind.semadec import imtensor
from bcfind.semadec import deconvolver
from multiprocessing import Process, Pipe, cpu_count, Value, Manager
from PIL import Image, TiffImagePlugin
from bcfind.utils import mkdir_p
from bcfind.log import tee
from bcfind import mscd
from bcfind import threshold
from bcfind import volume
from bcfind import timer
from bcfind import log

def safe_read(fd, size=1024):
   ''' reads data from a pipe and returns `None` on EAGAIN '''
   try:
      return os.read(fd, size)
   except OSError as exc:
      if exc.errno == errno.EAGAIN:
         return None
      raise

def core_usage(process, num_core):
    if (num_core > cpu_count() or num_core <= 0):
           exit(1)
    pid = process
    pid = str(pid)
    num_core = num_core - 1
    num_core = str(num_core)
    if (num_core == 0):
           os.system('taskset -cp 0 '+ pid)

    os.system('taskset -cp 0-'+ num_core +' '+ pid)

def main(args):
    # start a read/write comunication between processes
    parent, child = os.pipe()
    fl = fcntl.fcntl(parent, fcntl.F_GETFL)
    fcntl.fcntl(parent, fcntl.F_SETFL, fl | os.O_NONBLOCK)
    # extract general information of tensor
    parameters = json.loads(open(args.indir+'/info.json').read())
    width = parameters['Width']
    height = parameters['Height']
    # generate list of substacks
    get_tensor(args.margin, args.indir, width, height)
    sub.substacks = substacks
    indexes = sorted(substacks.keys())
    depth = len(substacks[indexes[1]]['names'])
    # if set save substacks to disc
    if args.save:
       save_substacks(indexes, args.indir)
    else:
       if not args.sync:
          # start parallel computing (limit process strategy)
          current = Value('d', 1)
          limit = args.limit
          for count in range(len(indexes)):
              if (indexes[count] != 'info'):
                 # comunication between processes
                 out_buffer=None
                 while current.value > limit:
                     #read from buffer
                     out_buffer = safe_read(parent, size = 1024)
                 q = Process(target = parallel_computing, args = (child, args.indir, indexes[count], args.model, args.trainfile, depth, height, width, count, current))
                 q.start()
                 current.value = current.value + 1 
          q.join()
       else:
          # start parallel computing (synchronized strategy)
          for count in range(len(indexes)):
              if (indexes[count] != 'info'):
                 if (count == 1):
                    p = Process(target = parallel_computing_sinc, args = (child, args.indir, indexes[count], args.model, args.trainfile, depth, height, width, count))
                    p.start()
                 # comunication between processes
                 out_buffer=None
                 while out_buffer != str(count):
                      #read from buffer
                      out_buffer = safe_read(parent, size = 1024)
                 if (count != len(indexes) - 1):
                      q = Process(target = parallel_computing_sinc, args = (child, args.indir, indexes[count + 1], args.model, args.trainfile, depth, height, width, count + 1))
                      q.start()
          q.join()

def parallel_computing(child, indir, index, model, trainfile, depth, height, width, count, current):
     # number of cores to use
     core_usage(os.getpid(), 15)
     semantic_deconv(indir, index, model, trainfile, depth, height, width)
     mean_shift(args, index)
     current.value = current.value - 1
     os.write(child, str(current))

def parallel_computing_sinc(child, indir, index, model, trainfile, depth, height, width, count):
    #number of cores to use
    core_usage(os.getpid(), 15)
    semantic_deconv(indir, index, model, trainfile, depth, height, width)
    # write on buffer
    os.write(child, str(count))
    core_usage(os.getpid(), 1)
    mean_shift(args, index)

def mean_shift(args, index):
    st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    tee.log('mean_shift running on', platform.node(), st)
    # extract substack index
    sub = substacks[index]
    ix = sub['Ix']
    iy = sub['Iy']
    ind = '%02d%02d' % (ix,iy) 
    mkdir_p(args.outdir+'/'+ind)
    if args.pair_id is None:
       tee.logto('%s/%s/log.txt' % (args.outdir,ind))
       args.outdir = args.outdir+'/'+ind
    else:
       tee.logto('%s/%s/log_%s.txt' % (args.outdir,ind,args.pair_id))
       args.outdir = args.outdir+'/'+ind
       mkdir_p(args.outdir)

    timers = [mscd.pca_analysis_timer, mscd.mean_shift_timer, mscd.ms_timer, mscd.patch_ms_timer]
    timers.extend([volume.save_vaa3d_timer, volume.save_markers_timer])
    timers.extend([threshold.multi_kapur_timer])

    for t in timers:
        t.reset()
    # start mean_shift analysis
    substack = volume.SubStack(args.indir, index, ram = 'yes')
    substack.load_volume_ram(index, substack, pair_id = args.pair_id)
    if args.local:
       mscd.pms(substack, args)
    else:
       mscd.ms(substack, args)
    for t in timers:
       if t.n_calls > 0:
          tee.log(t)
    st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    tee.log('mean_shift finished on', platform.node(), st)

def semantic_deconv(indir, substack_id, model, trainfile, depth, height, width):
    total_start = timeit.default_timer()
    print('Starting reconstruction of volume %s ...'%(str(substack_id)))
    substack = volume.SubStack(indir,substack_id, ram = 'yes')
    #np_tensor_3d, minz = imtensor.load_nearby_ram(substack, args.extramargin, depth, height, width)
    np_tensor_3d, minz = imtensor.load_nearby(args.tensorimage, substack, args.extramargin)    

    if not args.local_mean_std:
        print('Reading standardization data from', trainfile)
        h5 = tables.openFile(trainfile)
        Xmean = h5.root.Xmean[:].astype(np.float32)
        Xstd = h5.root.Xstd[:].astype(np.float32)
        h5.close()
    else:
        Xmean=None
        Xstd=None

    print('Starting semantic devonvolution of volume', str(substack_id))
    model = pickle.load(open(model))
    reconstruction = deconvolver.filter_volume(np_tensor_3d, Xmean, Xstd,
                                               args.extramargin, model, args.speedup, do_cython=args.do_cython, trainfile=trainfile)
    # save deconvolved images
    ix = substacks[substack_id]['Ix']
    iy = substacks[substack_id]['Iy']
    ind = '%02d%02d' % (ix,iy)
    save_deconvolved_images(reconstruction, substack_id)

    print ("total time reconstruction: %s" %(str(timeit.default_timer() - total_start)))

def get_tensor(margin, path, width, height):
    # extract files from source indir
    prefix = None
    current_path = os.getcwd()
    os.chdir(path)
    full_path = os.getcwd()
    os.chdir(current_path)
    files = os.listdir(full_path)
    files = sorted([f for f in files if f != 'info.json'])
    prefix = prefix or os.path.commonprefix(files)
    if prefix == '':
        raise Exception('Files in '+full_path+' do not have a common prefix ' + str(files))
    zrange = sorted([int(f.split('_')[-1].split('.')[0]) for f in files])
    n_digits = len((files[0].split('_')[-1].split('.')[0]))
    for elemento in zrange:
        if (not (elemento in range(zrange[0], zrange[-1]+1))):
            raise Exception('Files in '+full_path+' are not a complete sequence: '+str(zrange))
    prefix = '_'.join(prefix.split('_')[:-1])
    # create substacks from tensor
    depth = len(zrange)
    w = int(round(float((width-args.margin))/float(args.nx)))
    h = int(round(float((height-args.margin))/float(args.ny)))
    for i in range(args.nx):
        for j in range(args.ny):
            x0 = w*i
            y0 = h*j
            z0 = 0
            x1 = min(width, x0 + w + args.margin)
            y1 = min(height, y0 + h + args.margin)
            z1 = depth
            identifier = (i, j)
            substack = dict(Ix=i,Iy=j,Width=x1-x0, Height=y1-y0, Depth=z1-z0, X0=x0, Y0=y0, Z0=z0, Files=[], names=[])
            substacks[identifier] = substack
    for z_from_zero, z in enumerate(zrange):
        image_file = ('%s_%0'+str(n_digits)+'d'+args.suffix) % (prefix, z)
        img_z = Image.open(full_path+'/'+image_file)

        for substack_id, substack in substacks.iteritems():
            X0 = substack['X0']
            Width = substack['Width']
            Y0 = substack['Y0']
            Height = substack['Height']
            sub_image = img_z.crop((X0+0, Y0+0, X0+Width, Y0+Height))
            image = {
                 'pixels': sub_image.tobytes(),
                 'size': sub_image.size,
                 'mode': sub_image.mode,
            }
            substack['Files'].append(image)
            substack['names'].append(image_file)
    # save additional information
    info = dict(ImageSequenceDirectory=full_path,
                Width=width,
                Height=height,
                Depth=depth,
                Margin=margin)
    substacks['info'] = info

def save_deconvolved_images(np_tensor_3d, index):
    prefix = 'full_'
    pbar = ProgressBar(widgets=['Saving %d deconvolved images: ' % np_tensor_3d.shape[0], Percentage(), ' ', ETA()])
    for z in pbar(range(np_tensor_3d.shape[0])):
        out_img = Image.fromarray(np_tensor_3d[z,:,:])
        width, height = out_img.size
        image = {
             'pixels': out_img.tobytes(),
             'size': out_img.size,
             'mode': out_img.mode,
        }
        substacks[index]['Files'][z] = image
        #substacks[index]['Width'] = width
        #substacks[index]['Height'] = height

def save_substacks(indexes, path):
    new_path = path + '/substacks/'
    mkdir_p(new_path)
    subst = dict()
    for index in indexes:
         if (index != 'info'):
            substack = dict(Width = substacks[index]['Width'], Height=substacks[index]['Height'], Depth=substacks[index]['Depth'], X0=substacks[index]['X0'], Y0=substacks[index]['Y0'], Z0=substacks[index]['Z0'], Files=substacks[index]['names'])
            ind = '%02d%02d' % (substacks[index]['Ix'],substacks[index]['Iy'])
            subst[ind] = substack
            final_path = new_path + '/' +ind 
            mkdir_p(final_path)
            for f_from_zero, f in enumerate(substacks[index]['Files']):
                img = Image.frombytes(f['mode'], f['size'], f['pixels'])
                img.save(final_path + '/' + substacks[index]['names'][f_from_zero])
    print('Saving substack info into', new_path+'info.json')
    info = dict(ImageSequenceDirectory=path,
                Width=substacks['info']['Width'],
                Height=substacks['info']['Height'],
                Depth=substacks['info']['Depth'],
                Margin=substacks['info']['Margin'],
                SubStacks=subst,
                Full_Path_Of_Substacks=new_path)
    with open(new_path+'info.json', 'w') as ostream:
        print(json.dumps(info), file=ostream)

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('indir', type=str,
                        help='Directory contaning the source stack (sequence of JPG files)')
    parser.add_argument('outdir', metavar='outdir', type=str, help='Directory where prediction results will be saved')
    parser.add_argument('nx', type=int, help='# of substacks along the X dimension')
    parser.add_argument('ny', type=int, help='# of substacks along the Y dimension')
    parser.add_argument('tensorimage', metavar='tensorimage', type=str, help='H5 file containing a tensor')
    parser.add_argument('model', metavar='model', type=str,
                        help='pickle file containing a trained network')
    parser.add_argument('trainfile', metavar='trainfile', type=str,
                        help='HDF5 file on which the network was trained (should contain mean/std arrays)')
    parser.add_argument('-l','--local',dest='local',action='store_true',help='Perform local processing by dividing the volume in 8 parts.')
    parser.add_argument('-r', '--hi_local_max_radius', metavar='r', dest='hi_local_max_radius',
                        action='store', type=float, default=6,
                        help='Radius of the seed selection ball (r)')
    parser.add_argument('-t', '--seeds_filtering_mode', dest='seeds_filtering_mode',
                        action='store', type=str, default='soft',
                        help="Type of seed selection ball ('hard' or 'soft')")
    parser.add_argument('-R', '--mean_shift_bandwidth', metavar='R', dest='mean_shift_bandwidth',
                        action='store', type=float, default=5.5,
                        help='Radius of the mean shift kernel (R)')
    parser.add_argument('-si', '--save_image', dest='save_image', action='store_true',
                        help='Save debugging substack for visual inspection (voxels above threshold and colorized clusters).')
    parser.add_argument('-f', '--floating_point', dest='floating_point', action='store_true',
                        help='If true, cell centers are saved in floating point.')
    parser.add_argument('-mst', '--min_second_threshold', metavar='min_second_threshold', dest='min_second_threshold',
                        action='store', type=int, default=15,
                        help="""If the foreground (second threshold in multi-Kapur) is below this value
                        then the substack is too dark and assumed to contain no soma""")
    parser.add_argument('-M', '--max_expected_cells', metavar='max_expected_cells', dest='max_expected_cells',
                        action='store', type=int, default=10000,
                        help="""Max number of cells that may appear in a substack""")
    parser.add_argument('-p', '--pair_id', dest='pair_id',
                        action='store', type=str,
                        help="id of the pair of views, e.g 000_090. A folder with this name will be created inside outdir/substack_id")
    parser.add_argument('-limit', '--limit', type=int, default=50, help='Max number of process simultaneity')
    parser.set_defaults(save_image=False)
    parser.add_argument('-s', '--suffix', dest='suffix', type=str, default='.jpg', help='Image file suffix')
    parser.add_argument('-m', '--margin', dest='margin', type=int, default=40, help='Overlap between adjacent substacks')
    parser.add_argument('--extramargin', metavar='extramargin', dest='extramargin',
                        action='store', type=int, default=6,
                        help='Extra margin for convolution. Should be equal to (filter_size - 1)/2')
    parser.add_argument('--speedup', metavar='speedup', dest='speedup',
                        action='store', type=int, default=4,
                        help='convolution stride (isotropic along X,Y,Z)')
    parser.add_argument('--local_mean_std', dest='local_mean_std', action='store_true',
                        help='computcompute mean and std locally from the substack')
    parser.add_argument('--do_cython', dest='do_cython', action='store_true', help='use the compiled cython modules in deconvolver.py')
    parser.add_argument('-save', dest='save', action='store_true', help='save list of substacks to disk')
    parser.add_argument('-sync', dest='sync', action='store_true', help='start parallelization with synchronization instead of parallelization with an established number of processess')
    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
