#!/usr/bin/env python

from __future__ import print_function
import argparse
import json
import os
import subprocess
import uuid

from PIL import Image, TiffImagePlugin
from bcfind.utils import mkdir_p

def main(args):
    prefix = None
    savewd = os.getcwd()
    os.chdir(args.indir)
    full_path = os.getcwd()
    os.chdir(savewd)
    files = os.listdir(args.indir)
    files = [f for f in files if f[0] != '.' and f.endswith(args.suffix)]
    prefix = prefix or os.path.commonprefix(files)
    if prefix == '':
        raise Exception('Files in '+args.indir+' do not have a common prefix ' + str(files))
    zrange = sorted([int(f.split('_')[-1].split('.')[0]) for f in files])
    n_digits = len((files[0].split('_')[-1].split('.')[0]))
    if zrange != range(zrange[0], zrange[-1]+1):
        raise Exception('Files in '+args.indir+' are not a complete sequence: '+str(zrange))
    prefix = '_'.join(prefix.split('_')[:-1])
    for z in zrange:
        image_file = ('%s_%0'+str(n_digits)+'d'+args.suffix) % (prefix, z)
        img_z = Image.open(full_path+'/'+image_file)
        if z == zrange[0]:
            width, height = img_z.size
        else:
            if (width, height) != img_z.size:
                raise Exception('OOps, file', image_file, 'has size', img_z.size, 'instead of', (width, height))
    depth = len(zrange)
    tensors = dict()
    d = int(round(float((depth - args.margin)) / float(args.nz)))
    z0 = d * args.z_index
    z1 = min(depth, z0 + d + args.margin)
    tensor = dict(Depth = z1 - z0, Z0 = z0, Files = [])
    tensors['tensor'] = tensor
    for z_from_zero, z in enumerate(zrange):
        image_file = ('%s_%0'+str(n_digits)+'d'+args.suffix) % (prefix, z)
        for tensor_id, tensor in tensors.iteritems():
            Z0 = tensor['Z0']
            Depth = tensor['Depth']
            if z_from_zero in range(Z0, Z0+Depth):
                tensor['Files'].append(image_file)
    info = dict(Width=width,
                Height=height)
    with open(full_path+'/info.json','w') as ostream:
         print(json.dumps(info), file=ostream)
    tensor = tensors['tensor']
    files = tensor['Files']
    for f in files:
        os.system('rsync '+full_path+'/'+f+' '+args.server)        
    os.system('rsync '+full_path+'/info.json'+' '+args.server)

def get_parser():
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('indir', type=str,
                        help='Directory contaning the source stack')
    parser.add_argument('nz', type=int, help='# of tensors along the Z dimension')
    parser.add_argument('z_index', type=int, help='selected tensor (counted from zero)')
    parser.add_argument('server',type=str, help='destination server path')
    parser.add_argument('-s', '--suffix', dest='suffix', type=str, default='.jpg', help='Image file suffix')
    parser.add_argument('-m', '--margin', dest='margin', type=int, default=40, help='Overlap between adjacent tensors')
    return parser

if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args)
